 (function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

var imgs = new Array();

(function loadImgs() {
	for (let i = 0; i < 12; i++) {
		let ni = '000'+i;
		ni = ni.slice(-3);
		imgs[i] = new Image();
		imgs[i].src = '/home/kaze/MyDevelop/Web/Canvas/run/Sequences/Running/Running_'+ni+'.png';
		}
	}());
 
 function clearCanvas(canvas,ctx) {
	 // Сохраняем текущую матрицу трансформации
	 ctx.save();
	 // Используем идентичную матрицу трансформации на время очистки
	 ctx.setTransform(1, 0, 0, 1, 0, 0);
	 ctx.clearRect(0, 0, canvas.width, canvas.height);
	 // Возобновляем матрицу трансформации
	 ctx.restore();	 
	 }
 
 var i = 0;
 var canvas;
 var ctx;
 var start = Date.now();
  
 function run(timestamp) {
	 let progress = timestamp - start;
	 if (Math.abs(progress) > 50) {
		 clearCanvas(canvas,ctx);
		 ctx.drawImage(imgs[i],0,0,100,100);
		 i++;
		 if (i>=imgs.length) i = 0;
		 start = timestamp;
	 }
	 window.requestAnimationFrame(run);
	 }
	 
 window.onload = function() {

	 canvas = document.getElementById('draw');
	 ctx = canvas.getContext('2d');
	 window.requestAnimationFrame(run);
	 }
