canvas.on('click', function(e) {
    e.preventDefault();
    
    var mouse = {
        x: e.pageX - canvasPosition.x,
        y: e.pageY - canvasPosition.y
    }
    
    //do something with mouse position here
    
    return false;
});