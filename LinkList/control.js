function addRecord() {
	var group = document.getElementById('group').value;
	if (group=='') group = 'basic';
	var in_group = document.getElementById(group);
	if (in_group==null) {
		var newDiv	= document.createElement('div');
		var newP	= document.createElement('p');
		newP.innerHTML = group;
		newDiv.appendChild(newP);
		var newSel	= document.createElement('select');
		newSel.id	= group;
		newSel.multiple = 'multiple';
		newDiv.appendChild(newSel);
		var GroupBox = document.getElementById('GroupBox');
		GroupBox.appendChild(newDiv);
		in_group = document.getElementById(group);
	}
	var nameLink = document.getElementById('nameLink').value;
	var Link = document.getElementById('Link').value;
	var newOp	= document.createElement('option');
	newOp.innerHTML = nameLink;
	newOp.value = Link;
	in_group.appendChild(newOp);
}

function SaveList() {
	WinList = open("","displayWindow","width=500,height=400,status=yes,toolbar=yes,menubar=yes");
	WinList.document.open();
	// create document
	WinList.document.writeln('<!DOCTYPE html>');
	WinList.document.writeln('<html>');
	WinList.document.writeln('<head>');
	WinList.document.writeln('<title>Список ссылок</title>');
	WinList.document.writeln('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">');
	WinList.document.writeln('<meta charset="utf-8">');
	WinList.document.writeln('</head>');
	WinList.document.writeln('<body>');
	var divs = document.getElementsByTagName('div');
	for (var i = 0; i < divs.length; i++) {
		var sel_sel = divs[i].children[1];
		if (sel_sel.length==0) continue;
		var sel_p = divs[i].children[0];
		WinList.document.writeln('<h3>'+sel_p.innerHTML+'</h3>');
		WinList.document.writeln('<ul>');
		for (var j = 0; i < sel_sel.length; j++) {
			var sel_op = sel_sel.children[j];
			WinList.document.writeln('<li><a href="'+sel_op.value+'">'+sel_op.innerHTML+'</a></li>');
		}
		WinList.document.writeln('</ul>');
	}	
	WinList.document.writeln('</body>');
	WinList.document.write('</html>');
	WinList.document.close();
}

function onFilesSelect() {
	// проверяем поддерживает ли браузер file API
	if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
		// если нет, то предупреждаем, что демо работать не будет
		alert('К сожалению ваш браузер не поддерживает file API');
		return;
		}
	var files = document.getElementById('ExistList').files;
	var file = files[0];
	var reader = new FileReader();
    reader.onload = function(){
      var dataURL = reader.result;
      var output = document.createElement('textarea');
      output.innerHTML = dataURL;
	  var GroupBox = document.getElementById('GroupBox');
	  GroupBox.appendChild(output);
    };
    reader.readAsText(file);
}