var webdriver = require('selenium-webdriver');

var capabilities = {
    'browserName': 'FireFox'
  };
var entrytoEdit = "Browser Stack";

async function run() {
    var browser = new webdriver.Builder().withCapabilities(capabilities).build();
    await browser.get('//todomvc.com/examples/react/#');
    await browser.manage().timeouts().implicitlyWait(1000);
    const todoEntry = await browser.findElement(webdriver.By.css('.new-todo'));
    var toDoTestItems = [entrytoEdit, "Test Value1", "Test Value2"];
    //Send keystrokes to the field we just found with the test strings and then send the Enter special character
    for (const item of toDoTestItems) {
        await todoEntry.sendKeys(item);
        await todoEntry.sendKeys("\n");
        }
    }

run();
