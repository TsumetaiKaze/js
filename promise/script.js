function Go_OnClick() {
	alert('GO!');
	}

function delay(time=2000,setParam='Можем нАчать') {
	return new Promise(function(resolve, reject) {
		setTimeout(function() {
			resolve(setParam);
			}, time);
		});	
	}
	
function createHello(value) {
	let p = document.createElement("p");
	p.innerHTML = "hello";
	document.body.appendChild(p);
	return delay();
	}

function createWorld(value) {
	let p = document.createElement("p");
	p.innerHTML = "world";
	document.body.appendChild(p);
	return delay();
	}

function createBtnGo(value) {
	let input = document.createElement("input");
	input.type = "button";
	input.value = "Go!";
	input.setAttribute("onclick", "Go_OnClick()");
	document.body.appendChild(input);
	}

function searchHello() {
	let elements = document.querySelectorAll('p');
	let searchEl = false;
	for (let elem of elements) {
		if (elem.innerHTML=="hello") {
			console.log('Нашел параграф hello');
			searchEl = elem;
			break;
			}
		}	
	return delay(100,searchEl);
	}

function searchWorld(value) {
	if (value==false) return delay(100,value);
	
	let elements = document.querySelectorAll('p');
	let searchEl = false;
	for (let elem of elements) {
		if (elem.innerHTML=="world") {
			console.log('Нашел параграф world');
			searchEl = elem;
			break;
			}
		}	
	return delay(100,searchEl);
	}

function searchBtnGo(value) {
	if (value==false) return delay(100,value);
	
	let element = document.querySelector('input[type="button"][value="Go!"]');
	if (element==null) return delay(100,false);
		else {
			console.log('Нашел кнопку Go');
			return delay(100,element);
			}
	}

function search() {
	searchHello().then(searchWorld).then(searchBtnGo).then(DoAftherSearch);
	}

function DoAftherSearch(value) {
	if (value==false) {
		search();
		return false;
		}
	value.click();
	}

function window_OnLoad() {
	search();
	delay().then(createHello).then(createWorld).then(createBtnGo);
	}
	
window.onload = window_OnLoad();
