function hiddenBanner() {
	//скрываем наш баннер установив свойство hidden
	var banner	= document.getElementById('shi_wai_de_hengfu');
	banner.hidden	= true;
}

onload	= function() {
	//как только страница загрузилась и DOM построен добавляем баннер в контейнер div
	//здесь может быть любая структура баннера главное чтоб потом это хорошо смотрелось на сайте
	var banner	= document.getElementById('shi_wai_de_hengfu');//получаем баннер
	banner.style.backgroundColor = 'red';//устанавливаем цвет фона
	banner.style.color = 'white';//устанавливаем цвет текста
	banner.style.textAlign = 'center';//центруем содержимое
	
	var txt	= document.createElement('span');//добавляем текст баннера
	txt.innerHTML	= 'Ветрина оборудования и маетриалов для сварки от';
	banner.appendChild(txt);

	txt	= document.createElement('span');//добавляем фирму с форматированием круптный жирнывй текст со значком и отступом от кнопки
	txt.innerHTML	= 'Тиберис&#8482;';
	txt.style.fontWeight = 'bold';
	txt.style.fontStretch = 'ultra-expanded';
	txt.style.fontSize = 'large';
	txt.style.marginLeft = '1%';
	txt.style.marginRight = '20%';
	banner.appendChild(txt);
	
	var btn	= document.createElement('input');//добавляем конопку закрытия баннера
	btn.type	= 'button';
	btn.value	= 'Скрыть[X]';
	btn.onclick	= hiddenBanner;
	btn.style.backgroundColor = 'red';
	btn.style.color = 'white';
	
	banner.appendChild(btn);
}
